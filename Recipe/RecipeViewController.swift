//
//  RecipeViewController.swift
//  Recipe
//
//  Created by Sylvan .D. Ash on 20/02/2017.
//  Copyright © 2017 Daitensai. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController {
    
    // MARK: Variables and IBOutlets
    
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var prepTimeLabel: UILabel!
    @IBOutlet weak var ingredientsTextView: UITextView!
    
    var recipe: Recipe!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Navigation bar
        navigationItem.title = recipe.name
        
        // Recipe
        prepTimeLabel.text = recipe.prepTime
        recipeImageView.image = UIImage(named: recipe.imageFile)
        
        var ingredientText = ""
        for ingredient in recipe.ingredients {
            ingredientText += "\(ingredient)\n"
        }
        ingredientsTextView.text = ingredientText
    }

}
