//
//  Recipe.swift
//  Recipe
//
//  Created by Sylvan .D. Ash on 20/02/2017.
//  Copyright © 2017 Daitensai. All rights reserved.
//

import UIKit

class Recipe: NSObject {
    
    var name: String!
    var prepTime: String!
    var imageFile: String!
    var ingredients: [String]!
    
}
