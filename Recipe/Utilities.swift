//
//  Utilities.swift
//  Recipe
//
//  Created by Sylvan .D. Ash on 20/02/2017.
//  Copyright © 2017 Daitensai. All rights reserved.
//

import Foundation
import UIKit


/**
 Set title for the navigation bar's back button
 
 - parameter title: the title to display
 - returns: A Back button with the specified title on the navigation bar
 */
func backButtonTitle(title: String = "Back") -> UIBarButtonItem {
    let backItem = UIBarButtonItem()
    backItem.title = title
    backItem.tintColor = UIColor(red: 142, green: 68, blue: 173, alpha: 1) // Wisteria // Clouds: 236 / 240 / 241
    return backItem
}


/**
 Get an array of Recipes
 
 - returns: An array of recipes
 */
func loadRecipes() -> [Recipe] {
    
    let values = [
        ["name": "Egg Benedict", "time": "30 min", "file": "egg_benedict.jpg", "ingredients": ["2 fresh English muffins", "4 eggs", "4 rashers of back bacon", "2 egg yolks", "1 tbsp lemon juice", "125g of butter", "salt and pepper"]],
        ["name": "Mushroom Risotto", "time": "30 min", "file": "mushroom_risotto.jpg", "ingredients": ["1 tbsp dried porcini mushrooms", "2 tbsp olive oil", "1 onion, chopped", "2 garlic cloves", "350g / 12ox arborio rice", "1.2l / 2 pints hot vegetable stock", "salt and pepper", "25g / 1oz butter"]],
        ["name": "Full Breakfast", "time": "20 min", "file": "full_breakfast.jpg", "ingredients": ["2 sausages", "100g mushrooms", "2 rashers of bacon", "2 eggs", "150g baked beans", "vegetable oil"]],
        ["name": "Hamburger", "time": "30 min", "file": "hamburger.jpg", "ingredients": ["400g ground beef", "1/4 onion, minced", "1 tbsp butter", "hamburger bun", "1 tsp dry mustard", "salt and pepper"]],
        ["name": "Easy Tuna Casserole", "time": "45 min", "file": "tuna_casserole.jpg", "ingredients": ["3 cups cooked macaroni", "1 (5 ounce) can tuna, drained", "1 (10.75 ounce) can condensed cream of chicken soup", "1 cup shredded Cheddar cheese", "1 1/2 cups french fried onions"], "link": "http://allrecipes.com/recipe/18871/easy-tuna-casserole/"],
        ["name": "Easy Garlic Chicken", "time": "20 min", "file": "garlic_chicken.jpg", "ingredients": ["3 tbsp butter", "1 tsp seasoning salt", "4 skinless, boneless chicken breast halves", "2 tsp garlic powder", "1 tsp onion powder"], "link": "http://allrecipes.com/recipe/23998/a-good-easy-garlic-chicken/"],
        ["name": "Quick Beef Stir-Fry", "time": "25 min", "file": "beef_stir_fry.jpg", "ingredients": ["2 tbsp vegetable oil", "1 pound beef sirloin, cut into 2-inch strips", "1 1/2 cups fresh broccoli florets", "1 red bell pepper, cut into matchsticks", "2 carrots, thinly sliced", "1 green onion, chopped", "1 tsp minced garlic", "2 tbsp soy sauce", "2 tbsp sesame seeds, toasted"], "link": "http://allrecipes.com/recipe/228823/quick-beef-stir-fry/"],
        ["name": "Sweet and Sour Chicken", "time": "25 min", "file": "sweet_sour_chicken", "ingredients": ["3 tbsp all-purpose flour", "1/2 tsp garlic powder", "1/2 tsp salt", "1/2 tsp ground black pepper", "1 pound, skinless, boneless chicken breast halves, cut into 1-inch cubes", "3 tbsp vegetable oil, divided", "3 celery ribs, sliced", "2 green bell peppers, diced", "1 onion, chopped", "1/2 cup ketchup", "1/2 cup lemon juice", "1/2 cup crushed pineapple with syrup", "1/3 cup packd brown sugar"], "link": "http://allrecipes.com/recipe/220314/easy-sweet-and-sour-chicken/"],
        ["name": "Butter Chicken", "time": "55 min", "file": "butter_chicken.jpg", "ingredients": ["2 eggs, beaten", "1 cup crushed buttery round cracker crumbs", "1/2 tsp garlic salt", "ground black pepper to taste", "4 skinless, boneless chicken breast halves", "1/2 cup butter, cut into pieces"], "link": "http://allrecipes.com/recipe/24002/famous-butter-chicken/"],
        ["name": "Spicy Garlic Lime Chicken", "time": "25 min", "file": "garlic_lime_chicken.jpg", "ingredients": ["3/4 tsp salt", "1/4 tsp black pepper", "1/4 tsp cayenne pepper", "1/8 tsp paprika", "1/4 tsp garlic powder", "1/8 onion powder", "1/4 tsp dried thyme", "1/4 tsp dried parsley", "4 boneless, skinless chicken breast halves", "2 tbsp butter", "1 tbsp olive oile", "2 tsp garlic powder", "3 tbsp lime juice"], "link": "http://allrecipes.com/recipe/44868/spicy-garlic-lime-chicken/"],
        ["name": "Mushroom Pork Chops", "time": "40 min", "file": "mushroom_pork_chops.jpg", "ingredients": ["4 pork chops", "salt and pepper", "1 pinch garlic salt", "1 onion, chopped", "1/2 pound fresh mushrooms, sliced", "1 (10.75 ounce) can condensed cream of mushroom soup"], "link": "http://allrecipes.com/recipe/44868/spicy-garlic-lime-chicken/"],
        ["name": "Turkey Bacon Sandwich", "time": "7 min", "file": "turkey_bacon_sandwich.jpg", "ingredients": ["2 slices white bread", "1/4 cup mayonnaise", "3 lettuce leaves", "1 tomato, thinly sliced", "3 slices turkey bacon", "3 slices Cheddar cheese"], "link": "http://allrecipes.com/recipe/23827/amys-triple-decker-turkey-bacon-sandwich/"],
        ["name": "Easy Mustard-Glazed Broiled Salmon", "time": "15 min", "file": "mustard-glazed_broiled_salmon", "ingredients": ["1 tsp vegetable oil", "1 1/2 tbsp Dijon mustard", "2 tsp rice vinegar", "1/2 tsp Sriracha hot sauce (optional)", "2 (5 ounce) salmon fillets", "salt to taste"], "link": "http://allrecipes.com/recipe/239061/easy-mustard-glazed-broiled-salmon/"],
        ["name": "Eggplant Burgers", "time": "14 min", "file": "eggplant_burgers.jpg", "ingredients": ["1 eggplant, peeled and sliced into 3/4 inch rounds", "1 tbsp margarine", "6 slices Monterey Jack cheese", "6 hamburger buns, split", "6 leaves lettuce", "6 slices tomato", "1/2 onion, sliced", "1/2 cup dill pickle slices", "1 (20 ounce) bottle ketchup", "3 tbsp mayonnaise", "2 tbsp prepared yellow mustard"], "link": "http://allrecipes.com/recipe/53577/eggplant-burgers/"]
    ]
    
    var recipes = [Recipe]()
    for val in values {
        let recipe = Recipe()
        recipe.name = val["name"] as! String
        recipe.prepTime = val["time"] as! String
        recipe.imageFile = val["file"] as! String
        recipe.ingredients = val["ingredients"] as! [String]
        
        recipes.append(recipe)
    }
    
    return recipes
    
}
