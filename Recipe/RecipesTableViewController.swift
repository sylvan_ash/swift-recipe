//
//  RecipesTableViewController.swift
//  Recipe
//
//  Created by Sylvan .D. Ash on 20/02/2017.
//  Copyright © 2017 Daitensai. All rights reserved.
//

import UIKit

class RecipesTableViewController: UITableViewController {
    
    private var recipes: [Recipe] = []
    private var selectedRecipe: Recipe?
    let showRecipeNID = "ShowRecipeNID"
    

    override func viewDidLoad() {
        super.viewDidLoad()

        recipes = loadRecipes()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recipes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let recipe = recipes[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath)
        cell.textLabel?.text = recipe.name
        print(recipe.name)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedRecipe = recipes[indexPath.row]
        self.performSegue(withIdentifier: self.showRecipeNID, sender: self)
    }

    
    // MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if identifier == self.showRecipeNID && self.selectedRecipe != nil {
            return true
        }
        
        return false
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == self.showRecipeNID {
            let vc = segue.destination as! RecipeViewController
            vc.recipe = self.selectedRecipe!
        }
        
        // Back button
        navigationItem.backBarButtonItem = backButtonTitle()
    }

}
